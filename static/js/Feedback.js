Feedback = function(feedbackDesc) {
	this._feedback = feedbackDesc;
	this.domNode = null;
}

Feedback.prototype.getHTMLNode = function() {
	return this.domNode || (this.domNode = this.render());
}

Feedback.prototype.render = function() {
	var span = document.createElement("span");
	span.innerHTML = "By: "+this._feedback.user+"<div>"+this._feedback.feedback+"</div>";
	return span;
}

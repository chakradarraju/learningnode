var feedbackList = document.getElementById("feedbackList");

$.get('/showFeedback', function(feedbacks) {
	for(var i=0;i<feedbacks.length;i++) {
		feedbackList.appendChild(new Feedback(feedbacks[i]).getHTMLNode());
	}
}, "json");

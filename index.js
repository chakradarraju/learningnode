var server = require('./server'),
	router = require('./router'),
	static = require('./modules/static'),
	db = require('./modules/db');

db.init('mongodb://localhost/test');
feedback = db.getClass({id:Number,objId:Number,feedback:String,user:String});

router.registerAlias("/","/static/form.html");
router.registerHandler("/saveFeedback",feedback.saver());
router.registerHandler("/showFeedback",feedback.shower());
router.registerHandler("/static/*",static.pathHandler('./static'));

server.start(router.route);

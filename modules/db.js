var mongoose = require('mongoose'),
	qs = require('querystring');

exports.init = function(db) {
	mongoose.connect(db);
	mongoose.connection.on('error', console.error.bind(console, 'connection error!!'));
}

exports.getClass = function(desc) {
	var schema = mongoose.Schema(desc),
		cls = mongoose.model('cls',schema);
	function extractFields(post) {
		var ret = {};
		for(var key in desc) if(post[key]) ret[key] = post[key];
		return ret;
	}
	return {
		saver: function() {
			return function(req,res) {
				if(req.method === 'POST') {
					var body = '';
					req.on("data", function(data) {
						body += data;
					});
					req.on("end", function() {
						var newData = new cls(extractFields(qs.parse(body)));
						newData.save(function(err,newData) {
							if(err) res.end("Error in saving data");
							else res.end("Thank you for the response, Data saved successfully");
						});
					});
				}
			}
		},
		shower: function() {
			return function(req,res) {
				cls.find(function(err,objs) {
					if(err) res.end("Error in reading objects");
					else res.end(JSON.stringify(objs));
				});
			}
		}
	}
}

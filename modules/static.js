var fs = require('fs');

exports.handler = function(url) {
	var staticFile = "";

	fs.readFile(url, function(err, content) {
		if(err) staticFile = "Content Unavailable";
		else staticFile = content;
	});

	return function(req,res) {
		res.end(staticFile);
	}
}

exports.pathHandler = function(url) {
	return function(req,res) {
		fs.readFile('.'+req.url, function(err, content) {
			if(err) res.end("<h1>Error 404</h1>Page not found");
			else res.end(content);
		});
	}
}

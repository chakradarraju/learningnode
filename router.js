var handlers = {},
	preHandlers = [],
	aliases = {};

exports.route = function(req,res) {
	if(aliases[req.url]) req.url = aliases[req.url];
	for(var i=0;i<preHandlers.length;i++) {
		if(req.url.indexOf(preHandlers[i].url) === 0) {
			preHandlers[i].handler(req,res);
			return;
		}
	}
	if(handlers[req.url]) handlers[req.url](req,res);
	else res.end("<h1>404 Error</h1>Page not found");
}

exports.registerHandler = function(url,handler) {
	if(url[url.length - 1] === '*') preHandlers.push({url:url.substring(0,url.length-1),handler:handler});
	else handlers[url] = handler;
}

exports.registerAlias = function(url1,url2) {
	aliases[url1] = url2;
}

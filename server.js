var http = require("http"),
	port = 8888;

process.on('uncaughtException',function(err) { 
	if(err.code === "EADDRINUSE")
		console.log("Port " + port + " in use, can't start server");
	else
		console.log(err);
});

exports.start = function(route) {
	var server = http.createServer(function(request, response) {
		console.log("Serving: " + request.url);
		route(request,response);
	}).listen(port);
	
	console.log("Server started!");
};
